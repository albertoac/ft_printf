/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_hexa.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 14:41:57 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 15:51:11 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

int		ft_write_hex_hh(t_data data, t_flags flags)
{
	data.ui = data.uc;
	return (ft_write_hex(data, flags));
}

int		ft_write_hex_h(t_data data, t_flags flags)
{
	data.ui = data.ush;
	return (ft_write_hex(data, flags));
}

int		ft_write_long_hex(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	if (has_flag(flags.flag, '#') && data.ui != 0)
	{
		flags.width = flags.width - 2;
		sum = ft_write_str("0x");
	}
	sum = sum + ft_write_base(data.ull, 16, 'a', &flags);
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	return (sum);
}

int		ft_write_hex(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	if (has_flag(flags.flag, '#') && data.ui != 0)
		flags.width = flags.width - 2;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - u_length(data.ui, 16, flags));
	if (has_flag(flags.flag, '#') && data.ui != 0)
		sum = sum + ft_write_str("0x");
	sum = sum + ft_write_base((unsigned long long)data.ui, 16, 'a', &flags);
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	return (sum);
}
