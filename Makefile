# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/10 14:17:09 by aacuna            #+#    #+#              #
#    Updated: 2016/01/14 12:19:37 by aacuna           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRCS = ft_printf.c				\
	   ft_print_number.c		\
	   ft_print_hexa.c			\
	   ft_print_hexa_upper.c	\
	   ft_print_text.c 			\
	   ft_print_pointer.c		\
	   ft_printf_flags.c		\
	   ft_printf_text_utils.c	\
	   ft_print_base.c			\
	   ft_print_decimal.c		\
	   ft_print_octal.c			\
	   libft/ft_atoi.c			\
	   libft/ft_isdigit.c		\
	   libft/ft_strlen.c		\
	   libft/ft_strncmp.c		\

CFLAGS = -Wall -Wextra -Werror -I libft/

LIBRARY = libft/libft.a

OBJS = $(SRCS:.c=.o)

$(NAME)	:	$(OBJS)
			make -C libft/
			ar rc $(NAME) $(OBJS) $(LIBRARY)
			ranlib $(NAME)

all		:	$(NAME)

clean	:
			rm -rf $(OBJS)

fclean	:	clean
			rm -rf $(NAME)

re		:	fclean all
