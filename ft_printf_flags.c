/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_flags.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 10:18:00 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 10:34:08 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

int		is_flag(char c)
{
	int i;

	i = 0;
	while (g_flags[i] && g_flags[i] != c)
		i++;
	return (g_flags[i] != 0);
}

char	*add_flag(char *str, char flag)
{
	int i;

	i = 0;
	while (str[i] && str[i] != flag)
		i++;
	if (!str[i])
	{
		str[i] = flag;
		str[i + 1] = 0;
	}
	return (str);
}

int		has_flag(char *flags, char flag)
{
	int i;

	i = 0;
	while (flags[i] && flags[i] != flag)
		i++;
	return (flags[i] != 0);
}

t_flags	parse_flags(const char *str, int *i)
{
	int		pos;
	t_flags	flag;

	pos = 0;
	flag.flag[0] = '\0';
	while (is_flag(str[pos]))
	{
		add_flag(flag.flag, str[pos]);
		pos++;
	}
	flag.width = ft_atoi(&str[pos]);
	while (ft_isdigit(str[pos]))
		pos++;
	if (str[pos] == '.')
	{
		flag.precision = ft_atoi(&str[++pos]);
		while (ft_isdigit(str[pos]))
			pos++;
	}
	else
		flag.precision = -1;
	*i = *i + pos + 1;
	return (flag);
}
