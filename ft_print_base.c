/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_base.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 11:13:50 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 15:48:36 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char		get_hex_char(int nb, char letter)
{
	if (nb < 10)
		return (nb + '0');
	else
		return (nb - 10 + letter);
}

int			ft_write_base(uintmax_t nb, int base, char letter, t_flags *flags)
{
	int sum;

	sum = 0;
	if (flags->precision == 0 && nb == 0)
		return (0);
	if (nb >= (uintmax_t)base)
	{
		flags->width--;
		flags->precision--;
		sum = ft_write_base(nb / base, base, letter, flags);
		sum = sum + ft_write_char(get_hex_char(nb % base, letter));
		return (sum);
	}
	else
	{
		flags->width--;
		flags->precision--;
		if ((has_flag(flags->flag, '0') && flags->width > 0
			&& !has_flag(flags->flag, '-')) || flags->precision > 0)
			sum = ft_write_base(nb / base, base, letter, flags);
		return (sum + ft_write_char(get_hex_char(nb % base, letter)));
	}
}

uintmax_t	u_abs(intmax_t nb)
{
	uintmax_t res;

	res = nb;
	if (nb < 0)
		res = -nb;
	return (res);
}

int			u_length(uintmax_t nb, int base, t_flags flags)
{
	int i;

	i = 1;
	if (flags.precision == 0 && nb == 0)
		return (0);
	while (nb >= (uintmax_t)base)
	{
		i++;
		nb = nb / base;
	}
	if (has_flag(flags.flag, '+') || has_flag(flags.flag, ' '))
		i++;
	if (flags.precision > i)
		i = flags.precision;
	else if (has_flag(flags.flag, '0') && (flags.width - i) > 0)
	{
		i = flags.width;
	}
	return (i);
}

int			nb_length(intmax_t nb, t_flags flags)
{
	if (nb < 0)
	{
		return (1 + u_length(u_abs(nb), 10, flags)
				- (has_flag(flags.flag, '+') || has_flag(flags.flag, ' ')));
	}
	else
		return (u_length(u_abs(nb), 10, flags));
}
