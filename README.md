# ft_printf #

ft_printf is a clone of the printf C function. To use it, first you have to compile the library using the make command. It works exactly as the original printf. It supports:

* Variable conversions sSpdDioOuUxXcC
* %%
* Flags #0-+ and space
* Field size
* Precision
* Flags hh h l ll j z