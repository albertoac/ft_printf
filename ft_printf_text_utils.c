/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_text_utils.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 10:40:47 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 15:57:24 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

int	ft_write_char(int l)
{
	unsigned char c;

	c = (unsigned char)l;
	write(1, &c, 1);
	return (1);
}

int	ft_write_str(const char *str)
{
	int i;

	i = 0;
	if (str == NULL)
		return (ft_write_str("(null)"));
	else
		while (str[i] != '\0')
		{
			ft_write_char(str[i]);
			i++;
		}
	return (i);
}

int	len_str(char *str, t_flags flags)
{
	int length;

	length = ft_strlen(str);
	if (flags.precision > -1 && length > flags.precision)
		length = flags.precision;
	return (length);
}

int	ft_write_unicode_char(wchar_t c, int sum)
{
	if (c <= 0x7F)
		sum = sum + ft_write_char(c);
	else if (c <= 0x7FF)
	{
		sum = sum + ft_write_char((c / 0x40) + 0xC0);
		sum = sum + ft_write_char((c % 0x40) + 0x80);
	}
	else if (c <= 0xFFFF)
	{
		sum = sum + ft_write_char((c / 0x1000) + 0xE0);
		sum = sum + ft_write_char(((c % 0x1000) / 0x40) + 0x80);
		sum = sum + ft_write_char((c % 0x40) + 0x80);
	}
	else if (c <= 0x1FFFFF)
	{
		sum = sum + ft_write_char((c / 0x40000) + 0xF0);
		sum = sum + ft_write_char(((c % 0x40000) / 0x1000) + 0x80);
		sum = sum + ft_write_char(((c % 0x1000) / 0x40) + 0x80);
		sum = sum + ft_write_char((c % 0x40) + 0x80);
	}
	else
		return (-1);
	return (sum);
}

int	unicode_str_length(wchar_t *str, t_flags flags)
{
	int sum;
	int i;

	i = 0;
	sum = 0;
	if (str == NULL)
		return (6);
	while (str[i] != 0)
	{
		if (str[i] <= 0x7F)
			sum = sum + 1;
		else if (str[i] <= 0x7FF)
			sum = sum + 2;
		else if (str[i] <= 0xFFFF)
			sum = sum + 3;
		else if (str[i] <= 0x1FFFFF)
			sum = sum + 4;
		i++;
	}
	if (flags.precision == 0)
		sum = 0;
	return (sum);
}
