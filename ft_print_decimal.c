/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_decimal.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 11:22:57 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 11:31:22 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_write_unsig_long_dec(t_data data, t_flags flags)
{
	return (ft_write_base(data.uli, 10, 'a', &flags));
}

int	ft_write_nb_hh(t_data data, t_flags flags)
{
	data.i = data.c;
	return (ft_write_nb(data, flags));
}

int	ft_write_nb_h(t_data data, t_flags flags)
{
	data.i = data.sh;
	return (ft_write_nb(data, flags));
}

int	ft_write_unsigned_nb_hh(t_data data, t_flags flags)
{
	data.ui = data.uc;
	return (ft_write_unsigned_nb(data, flags));
}

int	ft_write_unsigned_nb_h(t_data data, t_flags flags)
{
	data.ui = data.ush;
	return (ft_write_unsigned_nb(data, flags));
}
