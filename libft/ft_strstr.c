/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 16:24:14 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/01 16:28:10 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	char	*str1;
	char	*str2;
	int		i;
	int		j;

	str1 = (char*)s1;
	str2 = (char*)s2;
	i = 0;
	if ((*s1 == '\0') && (*s2 == '\0'))
		return (str1);
	while (str1[i] != '\0')
	{
		j = 0;
		while ((str1[i + j] == str2[j]) && (str2[j] != '\0'))
			j++;
		if (str2[j] == '\0')
			return (str1 + i);
		i++;
	}
	return (NULL);
}
