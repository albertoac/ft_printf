/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 16:28:12 by aacuna            #+#    #+#             */
/*   Updated: 2015/11/25 17:03:38 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	int		len1;
	int		len2;
	char	*res;
	int		i;

	len1 = (s1 == NULL) ? 0 : ft_strlen(s1);
	len2 = (s2 == NULL) ? 0 : ft_strlen(s2);
	res = (char*)malloc((len1 + len2 + 1) * sizeof(*res));
	if (res == NULL)
		return (NULL);
	i = 0;
	if (len1 > 0)
		while (s1[i] != '\0')
		{
			res[i] = s1[i];
			i++;
		}
	if (len2 > 0)
		while (s2[i - len1] != '\0')
		{
			res[i] = s2[i - len1];
			i++;
		}
	res[i] = '\0';
	return (res);
}
