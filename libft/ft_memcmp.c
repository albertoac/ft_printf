/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 10:37:55 by aacuna            #+#    #+#             */
/*   Updated: 2015/11/24 11:00:10 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char *tab1;
	unsigned char *tab2;

	tab1 = (unsigned char*)s1;
	tab2 = (unsigned char*)s2;
	while ((n > 0) && ((*tab1 - *tab2) == 0))
	{
		tab1++;
		tab2++;
		n--;
	}
	if (n == 0)
		return (0);
	else
		return (*tab1 - *tab2);
}
