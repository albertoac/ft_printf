/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 17:55:04 by aacuna            #+#    #+#             */
/*   Updated: 2015/12/07 10:47:44 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		get_table_length(char const *s, char c)
{
	int tab_len;
	int i;

	if (s == NULL)
		return (0);
	tab_len = 0;
	i = 0;
	while (s[i] != '\0')
	{
		while ((s[i] != '\0') && (s[i] == c))
			i++;
		while ((s[i] != '\0') && (s[i] != c))
			i++;
		if ((i > 0) && (s[i - 1] != c))
			tab_len++;
	}
	return (tab_len);
}

char			**ft_strsplit(char const *s, char c)
{
	int		tab_len;
	int		i;
	char	**res;
	int		j;

	tab_len = get_table_length(s, c);
	i = 0;
	j = 0;
	res = ft_memalloc((1 + tab_len) * sizeof(res));
	if ((res == NULL) || (s == NULL))
		return (NULL);
	while (s[i] != '\0')
	{
		while ((s[i] != '\0') && (s[i] == c))
			i++;
		tab_len = 0;
		while ((s[i + tab_len] != '\0') && (s[i + tab_len] != c))
			tab_len++;
		if (tab_len > 0)
			res[j] = ft_strsub(s, i, tab_len);
		j++;
		i = i + tab_len;
	}
	return (res);
}
