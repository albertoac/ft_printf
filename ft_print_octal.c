/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_octal.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/14 12:16:17 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 16:09:36 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_write_octal_hh(t_data data, t_flags flags)
{
	data.ui = data.uc;
	return (ft_write_octal(data, flags));
}

int		ft_write_octal_h(t_data data, t_flags flags)
{
	data.ui = data.ush;
	return (ft_write_octal(data, flags));
}

int		ft_write_long_octal(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - u_length(data.ui, 8, flags));
	if (has_flag(flags.flag, '#') && (data.ui != 0 || flags.precision == 0))
	{
		flags.width = flags.width - 1;
		sum = ft_write_char('0');
	}
	sum = sum + ft_write_base(data.uli, 8, 'a', &flags);
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	return (sum);
}

int		ft_write_octal(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	if (has_flag(flags.flag, '#') && (data.ui != 0 || flags.precision == 0))
		flags.width--;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - u_length(data.ui, 8, flags));
	if (has_flag(flags.flag, '#') && (data.ui != 0 || flags.precision == 0))
	{
		if (flags.precision != 0)
			flags.precision--;
		sum = sum + ft_write_char('0');
	}
	sum = sum + ft_write_base(data.ui, 8, 'a', &flags);
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	return (sum);
}
