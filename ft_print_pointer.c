/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_pointer.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 15:14:18 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 11:03:43 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

int	ft_write_pointer(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	flags.width = flags.width - 2;
	if (flags.width > 0 && !has_flag(flags.flag, '0')
						&& !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - u_length(data.ui, 16, flags));
	sum = sum + ft_write_str("0x");
	return (sum + ft_write_long_hex(data, flags));
}
