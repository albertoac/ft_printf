/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_text.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 14:54:05 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 16:01:33 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

int	ft_write_percent(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	flags.width--;
	data.i = 0;
	if (flags.width > 0 && has_flag(flags.flag, '0')
						&& !has_flag(flags.flag, '-'))
		while (flags.width > 0)
		{
			flags.width--;
			sum = sum + ft_write_char('0');
		}
	if (flags.width > 0 && !has_flag(flags.flag, '0')
						&& !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	sum = sum + ft_write_char('%');
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	return (sum);
}

int	universal_write_char(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - 1);
	sum = sum + ft_write_char(data.c);
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - 1);
	return (sum);
}

int	universal_write_unicode_char(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - 1);
	sum = sum + ft_write_unicode_char(data.wc, 0);
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - 1);
	return (sum);
}

int	ft_write_unicode_str(t_data data, t_flags flags)
{
	int i;
	int sum;

	i = 0;
	sum = 0;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width
								- unicode_str_length(data.ws, flags));
	if (data.ws == NULL)
		return (ft_write_str("(null)"));
	while (data.ws[i] != 0 && flags.precision != 0)
	{
		sum = sum + ft_write_unicode_char(data.ws[i], 0);
		i++;
	}
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width
								- unicode_str_length(data.ws, flags));
	return (sum);
}

int	universal_write_str(t_data data, t_flags flags)
{
	int sum;
	int i;

	sum = 0;
	i = 0;
	if (data.s == NULL)
		data.s = "(null)";
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - len_str(data.s, flags));
	while (data.s[i] != '\0' && flags.precision != i)
	{
		sum = sum + ft_write_char(data.s[i]);
		i++;
	}
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width - len_str(data.s, flags));
	return (sum);
}
