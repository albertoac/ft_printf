/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/28 10:37:06 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 16:43:29 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdarg.h>
# include <inttypes.h>

typedef struct			s_flags
{
	char				flag[6];
	int					width;
	int					precision;
}						t_flags;

typedef union			u_data
{
	int					i;
	unsigned int		ui;
	long int			li;
	unsigned long int	uli;
	unsigned long long	ull;
	char				c;
	unsigned char		uc;
	short				sh;
	intmax_t			mx;
	uintmax_t			umx;
	unsigned short		ush;
	char				*s;
	wchar_t				wc;
	wchar_t				*ws;
}						t_data;

typedef struct			s_type
{
	char				*type;
	int					(*f)(t_data, t_flags);
}						t_type;

int						has_flag(char *flags, char flag);
int						ft_printf(const char *restrict format, ...);
int						ft_write_char(int l);
int						ft_write_unicode_char(wchar_t c, int sum);
int						ft_write_str(const char *str);
int						unicode_str_length(wchar_t *str, t_flags flags);
int						len_str(char *str, t_flags flags);
int						formated(const char *str, va_list argp, int *i);
t_flags					parse_flags(const char *str, int *i);
int						u_length(uintmax_t nb, int base, t_flags flags);
int						nb_length(intmax_t nb, t_flags flags);
uintmax_t				u_abs(intmax_t nb);

int						universal_write_char(t_data data, t_flags flags);
int						universal_write_unicode_char(t_data data,
													t_flags flags);
int						ft_write_unicode_str(t_data data, t_flags flags);
int						universal_write_str(t_data data, t_flags flags);
int						ft_write_nb(t_data data, t_flags flags);
int						ft_write_max_nb(t_data data, t_flags flags);
int						ft_write_unsigned_nb(t_data data, t_flags flags);
int						ft_write_unsig_long_dec(t_data data, t_flags flags);
int						ft_write_long_octal(t_data data, t_flags flags);
int						ft_write_octal(t_data data, t_flags flags);
int						ft_write_long_hex(t_data data, t_flags flags);
int						ft_write_hex(t_data data, t_flags flags);
int						ft_write_upper_hex(t_data data, t_flags flags);
int						ft_write_pointer(t_data data, t_flags flags);
int						ft_write_percent(t_data data, t_flags flags);
int						ft_write_nb_hh(t_data data, t_flags flags);
int						ft_write_unsigned_nb_hh(t_data data, t_flags flags);
int						ft_write_octal_hh(t_data data, t_flags flags);
int						ft_write_hex_hh(t_data data, t_flags flags);
int						ft_write_upper_hex_hh(t_data data, t_flags flags);
int						ft_write_nb_h(t_data data, t_flags flags);
int						ft_write_unsigned_nb_h(t_data data, t_flags flags);
int						ft_write_octal_h(t_data data, t_flags flags);
int						ft_write_hex_h(t_data data, t_flags flags);
int						ft_write_upper_hex_h(t_data data, t_flags flags);
int						ft_write_long_upper_hex(t_data data, t_flags flags);

int						ft_write_base(uintmax_t nb, int base, char letter,
										t_flags *flags);
int						print_spaces(int nb_spaces);

static const t_type	g_funcs[] =
{
	{"c", &universal_write_char},
	{"C", &universal_write_unicode_char},
	{"s", &universal_write_str},
	{"S", &ft_write_unicode_str},
	{"d", &ft_write_nb},
	{"i", &ft_write_nb},
	{"u", &ft_write_unsigned_nb},
	{"D", &ft_write_max_nb},
	{"U", &ft_write_unsig_long_dec},
	{"o", &ft_write_octal},
	{"O", &ft_write_long_octal},
	{"x", &ft_write_hex},
	{"X", &ft_write_upper_hex},
	{"p", &ft_write_pointer},
	{"%", &ft_write_percent},
	{"hhd", &ft_write_nb_hh},
	{"hhD", &ft_write_max_nb},
	{"hhi", &ft_write_nb_hh},
	{"hhu", &ft_write_unsigned_nb_hh},
	{"hhU", &ft_write_unsig_long_dec},
	{"hho", &ft_write_octal_hh},
	{"hhO", &ft_write_long_octal},
	{"hhx", &ft_write_hex_hh},
	{"hhX", &ft_write_upper_hex_hh},
	{"hhC", &universal_write_unicode_char},
	{"hhS", &ft_write_unicode_str},
	{"hd", &ft_write_nb_h},
	{"hD", &ft_write_max_nb},
	{"hi", &ft_write_nb_h},
	{"hu", &ft_write_unsigned_nb_h},
	{"hU", &ft_write_unsig_long_dec},
	{"ho", &ft_write_octal_h},
	{"hO", &ft_write_octal_h},
	{"hx", &ft_write_hex_h},
	{"hX", &ft_write_upper_hex_h},
	{"lld", &ft_write_max_nb},
	{"llD", &ft_write_max_nb},
	{"lli", &ft_write_max_nb},
	{"llu", &ft_write_unsig_long_dec},
	{"llU", &ft_write_unsig_long_dec},
	{"llo", &ft_write_long_octal},
	{"llO", &ft_write_long_octal},
	{"llx", &ft_write_long_hex},
	{"llX", &ft_write_long_upper_hex},
	{"ld", &ft_write_max_nb},
	{"lD", &ft_write_max_nb},
	{"li", &ft_write_max_nb},
	{"lu", &ft_write_unsig_long_dec},
	{"lU", &ft_write_unsig_long_dec},
	{"lo", &ft_write_long_octal},
	{"lO", &ft_write_long_octal},
	{"lx", &ft_write_long_hex},
	{"lX", &ft_write_long_upper_hex},
	{"lc", &universal_write_unicode_char},
	{"ls", &ft_write_unicode_str},
	{"lp", &ft_write_pointer},
	{"jd", &ft_write_max_nb},
	{"jD", &ft_write_max_nb},
	{"ji", &ft_write_max_nb},
	{"ju", &ft_write_unsig_long_dec},
	{"jU", &ft_write_unsig_long_dec},
	{"jo", &ft_write_long_octal},
	{"jO", &ft_write_long_octal},
	{"jx", &ft_write_long_hex},
	{"jX", &ft_write_long_upper_hex},
	{"zd", &ft_write_max_nb},
	{"zD", &ft_write_max_nb},
	{"zi", &ft_write_max_nb},
	{"zu", &ft_write_unsig_long_dec},
	{"zU", &ft_write_unsig_long_dec},
	{"zo", &ft_write_long_octal},
	{"zO", &ft_write_long_octal},
	{"zx", &ft_write_long_hex},
	{"zX", &ft_write_long_upper_hex},
	{NULL, NULL}
};

static const char g_flags[] = {'#', '-', '+', ' ', '0', 0};
#endif
