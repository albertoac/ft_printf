/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_number.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 16:17:26 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 16:36:55 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

int	ft_write_unsigned_nb(t_data data, t_flags flags)
{
	int		sum;
	t_data	data_aux;

	sum = 0;
	data_aux.uli = (unsigned long int)data.ui;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = print_spaces(flags.width - nb_length(data.li, flags));
	sum = sum + ft_write_base(data_aux.uli, 10, 'a', &flags);
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	return (sum);
}

int	ft_write_nb(t_data data, t_flags flags)
{
	t_data data_aux;

	data_aux.li = (long int)data.i;
	return (ft_write_max_nb(data_aux, flags));
}

int	ft_write_max_nb(t_data data, t_flags flags)
{
	int sum;

	sum = 0;
	if ((has_flag(flags.flag, '+') && data.mx >= 0)
		|| has_flag(flags.flag, ' '))
		flags.width--;
	if (flags.width > 0 && !has_flag(flags.flag, '-'))
		sum = print_spaces(flags.width - nb_length(data.mx, flags));
	if (data.mx < 0)
	{
		sum = sum + ft_write_char('-');
		flags.width--;
		sum = (sum + ft_write_base(u_abs(data.umx), 10, 'a', &flags));
	}
	else
	{
		if (has_flag(flags.flag, '+'))
			sum = sum + ft_write_char('+');
		else if (has_flag(flags.flag, ' '))
			sum = sum + ft_write_char(' ');
		sum = (sum + ft_write_base(data.umx, 10, 'a', &flags));
	}
	if (flags.width > 0 && has_flag(flags.flag, '-'))
		sum = sum + print_spaces(flags.width);
	return (sum);
}
