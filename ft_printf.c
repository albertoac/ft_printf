/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aacuna <aacuna@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/28 10:11:55 by aacuna            #+#    #+#             */
/*   Updated: 2016/01/14 15:19:21 by aacuna           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "libft.h"

int	print_spaces(int nb_spaces)
{
	int sum;

	sum = 0;
	while (nb_spaces > 0)
	{
		sum = sum + ft_write_char(' ');
		nb_spaces--;
	}
	return (sum);
}

int	formated(const char *str, va_list argp, int *i)
{
	int		pos;
	t_data	data;
	t_flags	flags;

	pos = 0;
	data.i = 0;
	flags = parse_flags(&str[*i + 1], i);
	while (g_funcs[pos].type && ft_strncmp(g_funcs[pos].type, &str[*i],
										ft_strlen(g_funcs[pos].type)))
	{
		pos++;
	}
	if (g_funcs[pos].type)
	{
		*i = *i + ft_strlen(g_funcs[pos].type) - 1;
		if (g_funcs[pos].type[0] != '%')
			data = va_arg(argp, t_data);
		return (g_funcs[pos].f(data, flags));
	}
	*i = *i - 1;
	return (0);
}

int	ft_printf(const char *restrict format, ...)
{
	va_list	argp;
	int		i;
	int		sum;

	i = 0;
	sum = 0;
	va_start(argp, format);
	while (format[i] != '\0')
	{
		if (format[i] == '%')
		{
			sum = sum + formated(format, argp, &i);
		}
		else
			sum = sum + ft_write_char(format[i]);
		i++;
	}
	va_end(argp);
	return (sum);
}
